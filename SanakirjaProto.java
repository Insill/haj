// package haj_de4;

public class SanakirjaProto {

    public final static String Tallenna = "TALLENNA";   // tallennuskomento
    public final static String Hae = "HAE";             // hakukomento
    public final static String Ok = "200 OK";               // onnistunut tallennus
    public final static String Tyhja = "500 EIKELPAA";      // sana tai selitys ei kelpaa
    public final static String EiLoydy = "400 TUNTEMATONSANA";  // tuntematon sana
    public final static String EnYmmarra = "500 TUNTEMATONKOMENTO"; // tuntematon komento
    public final static String EOL = "\r\n";            // rivinloppu

}
