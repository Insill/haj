import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ThreadPalvelin implements Runnable {

	ReadWriteLock lock = new ReentrantReadWriteLock(); // Lukko
    public static ServerSocket ss = null; // Asiakkaan ja palvelimen t�pselit
    public Socket cs;
    public SanakirjaProto sanaKirjaProto = new SanakirjaProto();
    // sanakirjan tallennuspaikka
    TreeMap<String, String> data = null;
    // tulostusten m��r� 0 = ei tulostuksia, 1 = jotain, 2 = enemm�n
    int print = 2;

    ThreadPalvelin(Socket cs) {
    	this.cs = cs;
    }

    public static void main(String[] args) {

        ThreadPalvelin p;
        int pr = 1;

        if (args.length > 1)    // tulostusten m��r�: toinen komentoriviparametri
            pr = Integer.valueOf(args[1]);

        if (args.length > 0)
            p = new ThreadPalvelin(Integer.valueOf(args[0]), pr);
        else
            p = new ThreadPalvelin(pr);


        if (ThreadPalvelin.ss == null)   // poistutaan jollei soketti auennut
            return;

        p.kuuntele();

    } // main()

        public ThreadPalvelin(int port, int print) {

        this.print = print;

        try {
            ss = new ServerSocket(port);
            if (print > 0)
                System.out.println("Kuunnellaan porttia " + port);
        } catch (Exception e) {
            System.err.println(e);
            ss = null;
        }
        if (ss != null)
            data = new TreeMap<String, String>();
    }

    public ThreadPalvelin(int print) {
        this(1234, print);
    }

    public void kuuntele() {

        try {
            while (true) {
                // odotetaan uutta yhteytt�
                Socket cs = ss.accept(); // cs eli ClientSocket
                // palvellaan yhteys
                new Thread(new ThreadPalvelin(cs)).start();
            }
        } catch (Exception e) {
            System.err.println(e);
            ss = null;
        }
    }
	@Override // palvellaan yhteys s�ikeistettyn�
	public void run() {
	 try {
		cs.setSoTimeout(20000);
		
        if (print > 0)
            System.out.println("Uusi yhteys: " + cs.getInetAddress() +
                               ":" + cs.getPort());

        // virrat k�ytt�kelpoiseen muotoon
        Scanner in = new Scanner(cs.getInputStream());
        PrintWriter out = new PrintWriter(cs.getOutputStream(), true);

		while(in.hasNext() && !cs.isClosed()) { // Jos sy�tett� on ja yhteys ei ole katki, luetaan sy�te
        // luetaan komento
	        String komento = in.nextLine();
	        System.out.println(komento);
	        if (komento.startsWith(SanakirjaProto.Tallenna)) {
	            // uuden sanan tallennus
	        	lock.writeLock().lock();	// Kun sanat luetaan, lukitaan muilta s�ikeilt� p��sy
	            String sana = in.nextLine().trim();
	            String selitys = in.nextLine().trim();
	            if (sana.length() == 0 || selitys.length() == 0) {
	                out.print(SanakirjaProto.Tyhja + SanakirjaProto.EOL);
	                if (print > 0) System.out.println("Tallennuksessa tyhj� sana tai selitys");
	            } else {
	                data.put(sana, selitys);
	                lock.writeLock().unlock(); // Avataan lukko kun sana on selityksineen saatu talteen
	                out.print(SanakirjaProto.Ok + SanakirjaProto.EOL);
	                if (print > 1) System.out.println("Tallennettiin |" + sana + ":" + selitys + "|");
	                if (print > 1) System.out.println("Lahetettiin |" + SanakirjaProto.Ok + SanakirjaProto.EOL + "|");
	            }

	        } else if (komento.startsWith(SanakirjaProto.Hae)) {
	            // hakeminen
	            String sana = in.nextLine().trim();
	            String selitys = data.get(sana);
	            if (selitys == null) {
	                out.print(SanakirjaProto.EiLoydy + SanakirjaProto.EOL);
	                if (print > 1) System.out.println("Sanaa |" + sana + "| ei l�ydetty");
	            } else {
	                out.print(selitys + SanakirjaProto.EOL);
	                if (print > 1) System.out.println("L�hetettiin |" + selitys + "|");
	            }

	        } else {
	            // tuntematon komento
	            out.print(SanakirjaProto.EnYmmarra + SanakirjaProto.EOL);
	            if (print > 0) System.out.println("Tuntematon komento |" + komento + "|");
	        }
		}
        if(!cs.isConnected() && cs.isClosed()){
        	in.close();
        	out.flush();
        	System.out.println("Yhteys " + cs.getInetAddress() +
                               ":" + cs.getPort() + " lopetettu.");
        	cs.close();
        }

    } catch (Exception e) {
        e.printStackTrace();
        try {
            // poikkeus saattoi tulla my�s Scanner:sta, joten suljetaan
            // socketti.
            cs.close();
        } catch (Exception e2) {
        	e2.printStackTrace();
        }
    }


	}   // kuuntele()

}


