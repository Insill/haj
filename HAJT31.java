/*HAJ teht�v� 31
 * 
 * 
 * 
 */
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;
import javax.json.*;
public class HAJT31 {
	
	static double windSpeed; 
	static double temperature; 
	static double windChill; 
	static Scanner scanner = new Scanner(System.in);
	static String city; 
	static String weatherURL = "http://api.openweathermap.org/data/2.5/weather";
    static String unit = "&units=metric";
    static String form = ""; 
    static String appId = "&appid=681fb05c8a442daf7087d168adc747c4";    //sjuva
    
	public static void main(String[] args){
		
		System.out.print("Anna kaupunki ja maa (esim. joensuu,fi) : ");
		city = scanner.nextLine();
		scanner.close();
		URL url = null; 
		try {
			String u = weatherURL + "?" + "q=" + city + appId + unit; // Esimerkiss� oli laitettu muuttujana my�s datan muoto, mik� on turhaa koska oletusarvoisesti se haetaan JSON-formaatissa
			url = new URL(u);
		} catch(Exception e) {
			e.printStackTrace();
		}
		try {
			InputStream is = url.openStream();
			JsonReader rdr = Json.createReader(is);
			JsonObject obj = rdr.readObject();
			JsonObject main = obj.getJsonObject("main");
			JsonObject wind = obj.getJsonObject("wind");
			JsonNumber temp = main.getJsonNumber("temp");
			JsonNumber speed = wind.getJsonNumber("speed");
			windSpeed = speed.doubleValue();
			temperature = temp.doubleValue();
			windChill = (int)(temperature - windSpeed); 
			System.out.println("Pakkasen purevuus: " + windChill + " C");
			is.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

}
