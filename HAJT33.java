import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

import javax.json.*;
public class HAJT33 {
	static Scanner scanner = new Scanner(System.in);
	static String city; 
	static String weatherURL = "http://api.openweathermap.org/data/2.5/forecast/daily";
	static String count = "&cnt=7"; 
	static int maxTemp; 
    static String unit = "&units=metric";
    static String appId = "&appid=681fb05c8a442daf7087d168adc747c4";    //sjuva
	public static void main(String[] args) {
		System.out.print("Anna kaupunki ja maa (esim. joensuu,fi) : ");
		city = scanner.nextLine();
		scanner.close();
		URL url = null; 
		try {
			String u = weatherURL + "?" + "q=" + city + unit + count + appId;
			url = new URL(u);
		} catch(Exception e) {
			e.printStackTrace();
		}
		try {
			InputStream is = url.openStream();
			JsonReader rdr = Json.createReader(is);
			JsonObject obj = rdr.readObject();
			JsonObject temp = obj.getJsonArray("list").getJsonObject(0).getJsonObject("temp");
			maxTemp = temp.getJsonNumber("max").intValue();
			System.out.println("Viikon korkein l�mp�tila: " + maxTemp + " C"); 
			is.close();
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}
}
