import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HAJT34 {
	
	public static void main(String[] args) {
		Calendar now = Calendar.getInstance();
		String today = now.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT,Locale.US); // Luodaan kalenteri-olio otetaan t�m� p�iv� lyhenteen�
		Document htmlFile = null; 
		Map<String,String> map = new HashMap<String, String>(7);
		Map<String,String> weekDays = new HashMap<String, String>(7);
		weekDays.put("MON", "MA"); // Tehd��n HashMap viikonp�ivist� englanniksi ja suomeksi
		weekDays.put("TUE","TI");
		weekDays.put("WED","KE");
		weekDays.put("THU", "TO"); 
		weekDays.put("FRI", "PE");
		weekDays.put("SAT", "LA");
		weekDays.put("SUN", "SU"); 
		
		try {
			htmlFile = Jsoup.connect("http://aukioloajat.com/kuopio/ruokakaupat/584591116").get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Elements days = htmlFile.getElementsByClass("weekday");		// Viikonp�iv�t
		Elements dates = htmlFile.getElementsByClass("opentime2");	// Kulloisenkin viikonp�iv�n aukioloajat
			for(Element day : days) {
				for(Element date: dates) {
					map.put(day.text().toString(), date.text().toString());	// Tehd��n HashMap p�ivist� ja aukioloajoista
				}
			}
		String day = weekDays.get(today.toUpperCase());
		System.out.println(day + " " + map.get(day));
		if(htmlFile.getElementById("curopen").text().isEmpty() == false) { 
			System.out.println(htmlFile.getElementById("curopen").text()); // N�ytt��, kuinka pitk��n kauppa on viel� auki
		}
	}

}
