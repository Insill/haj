import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import java.util.Scanner;

public class Asiakas {

	static int print = 1;
	public static Socket s = null;
	public static String address = "localhost";
	public static int port = 1234;
	static Scanner scanner = new Scanner(System.in);

	public static void main(String[]args){

		try {
			if(connect() == true) { // Jos yhdist�minen ei onnistunut, lopetetaan
				aloitus();
			}
			else{
				System.out.println("\nEi onnistuttu yhdist�m��n! Onko palvelin ylh��ll�?");
				lopeta();
			}
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (@SuppressWarnings("hiding") IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}
	public static void aloitus(){
		boolean valid;
		int choice = -2;
		do { // T�ss� sy�tteen kysyminen ei kuse
			System.out.println("Etsit��nk� sana (1) vai tallennetaanko uusi sana (2)? Voit my�s lopettaa (0) : ");
			choice = scanner.nextInt();
			valid = choice > -1 && choice < 3;
			if (!valid) {
				System.out.println("Anna valinta uudestaan, se on ep�kelpo!");
			}
		} while(!valid);

		try {
			switch(choice) {
				case 0:
				try {
					lopeta();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				case 1:
					search();
				case 2:
					tallenna();
			}
		} catch (Exception e) {
			System.out.println("\nVirhe:");
			e.printStackTrace();
			lopeta();
		}
	}
	public static boolean search() {

		try {
			String word;
			boolean valid;
			PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
      		do { // Sy�tteen kysyminen kusee t�ss�kin!
            	System.out.println("\nAnna haettava sana: ");
            	word = scanner.nextLine();
            	valid = word != "" && word.matches(".*[a-zA-Z]+.*");
            	if(!valid){
            		System.out.println("Ep�kelpo sy�te, anna haettava sana uudestaan!");
            	}
            } while(!valid);
            System.out.println(word);
            out.println(SanakirjaProto.Hae);
            out.println(word);

            String vastaus = in.readLine().trim();
            if (print > 1) System.out.println("Vastauksena saatiin |" + vastaus + "|");

            out.close();
            in.close();

            if (vastaus.startsWith("200"))
                return true;
            else
                return false;

        // poikkeusten k�sittely
       } catch (Exception e) {
            e.printStackTrace();
            if (s != null)
                try {
                    lopeta();
                } catch (Exception e2) {
                	e2.printStackTrace();
                }

            return false;
		}

	}
    public static boolean tallenna() {

        try {
            // luodaan keskustelukanavat
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            boolean valid;
            String word, explanation;
     		do { // Sy�tteen kysyminen kusee
            	System.out.println("Anna tallennettava sana: ");
            	word = scanner.nextLine();
            	System.out.println("Anna sanan selitys: ");
            	explanation = scanner.nextLine();
            	valid = (word != "" && word.matches(".*[a-zA-Z]+.*")) && (explanation != "" && explanation.matches("^(?=.*[A-Z])(?=.*[0-9])[A-Z0-9]+$"));
          		if(!valid) {
          			System.out.println("Sy�tteesi ovat ep�kelpoja, anna tallennettava sana ja sen selitys uudestaan!");
          		}
            } while(!valid);
            // l�het� viesti
            System.out.println(word + " : " + explanation);
            out.println(SanakirjaProto.Tallenna);
            out.println(word);
            out.println(explanation);

            // vastaanota kuittaus
            String vastaus = in.readLine().trim();

            if (print > 1) System.out.println("\nVastauksena saatiin |" + vastaus + "|");

            out.close();
            in.close();
            if (vastaus.startsWith("200"))
                return true;
            else
                return false;
        // poikkeusten k�sittely
        } catch (Exception e) {
            System.err.println(e);
            if (s != null)
                try {
                    return false;
                } catch (Exception e2) {
                	e2.printStackTrace();
                }

            return false;
        } // catch

    }   // tallenna()
    public static void lopeta(){
    	System.out.println("Lopetetaan...");
    	try {
    		s.close();
    	} catch(IOException e) {
    		e.printStackTrace();
    	}
    	System.exit(0);
    }	// Yhdist�minen palvelimelle ja socketin luonti
    public static boolean connect() throws SocketException {
        try {
            s = new Socket(address, port);     // yhteydenotto, palautetaan false jos ei onnistu
        } catch (Exception e) {
            e.printStackTrace();
            try {
				s.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            return false;
        }
        if (print > 0) {
        	System.out.println("Yhteys onnistui");
			return true;
        }
   		else {
   			return false;
   		}
	}

}
