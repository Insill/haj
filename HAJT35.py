
import json,re,urllib
from urllib.request import urlopen

def main():
	while True: 
		zipCode = input("Anna postinumero: ")
		if zipCode == "": 
			print("Ei syötettä!")   
		else:
			break
	url = "https://ohjelmat.posti.fi/pup/v1/pickuppoints?zipcode=" + zipCode
	response = urllib.request.urlopen(url)
	data = json.loads(response.read().decode(response.info().get_param("charset") or "utf-8"))
	print(data[0])
	
main()