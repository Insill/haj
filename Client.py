# encoding -*- utf-8 -*-
import socket, sys

class Client:

    def __init__ (self,host,port):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
        self.socket.settimeout(2000)
        self.connected = False
        self.host = host
        self.port = port 

    def connect(self): 
        try:
            self.socket.connect((self.host,self.port))
            print("\n---------\nReady! \n---------\n")  
            self.connected = True
        except socket.error:
            e = sys.exc_info()
            print("\n--------\nSocket error : {} \n-------\n".format(e))
            self.connected = False 
            self.quit()
            
        while self.connected:
            self.idle()
    
    def idle(self):
        while True: 
            choice =  int(input("Tallennetaanko [1] vai haetaanko [2] palvelimelta? Voit myös lopettaa [3]:\t"))
            if choice == 1:
                self.save()
            if choice == 2: 
                self.search()
            if choice == 3: 
                self.quit()
            else:
                print("Syöte ei kelpaa")
    
    def save(self):
        while True:
            word = input("Anna tallennettava sana: ")
            explanation = input("Anna sanan selitys: ")
            if word == "" or explanation == "": 
                print("Ei syötettä!")   
            else:
                break
        self.socket.send(("TALLENNA\n" + word + "\n" + explanation).encode())
        text = self.socket.recv(1000).decode()
        print(text)
        if text.find("200"):
            return True
        else:
            self.idle()
        
    def search(self):
        while True: 
            word = input("Anna haettava sana: ")
            break
            if word == "": 
                print("Ei syötettä!")   
        self.socket.send(("HAE\n" + word).encode())
        text = self.socket.recv(1000).decode()
        print(text)
        if text.find("200"):
            return True
        else:
            self.idle()
        
    def quit(self):
        print("Lopetetaan")
        self.connected = False
        self.socket.close()
        sys.exit()
    
def main():

    if len(sys.argv) >= 2:
        host = sys.argv[1]
        port = int(sys.argv[2])
    else:
        host = input("Anna osoite: ") 
        port = int(input("Anna portti: "))
    try: 
        print("\nYhdistetään kohteeseen " + host + " portissa " + str(port)) 
        client = Client(host,port)
        client.connect()
    except:
        e = sys.exc_info() # The new string formatting of Python
        print("\n--------\nAn error: {} \n--------\n".format(e)) 
        client.quit();
            
main()