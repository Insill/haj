
import java.util.Scanner;
import java.util.stream.Collectors;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
public class HAJT35 {
	
	static String postURL = "https://ohjelmat.posti.fi/pup/v1/pickuppoints?zipcode=";
	static int zipCode;
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Anna postinumero: ");
		zipCode = scanner.nextInt();
		scanner.close();
		URL url = null; 
		Document doc = null; 
		String temp = null; 
		try {
			String u = postURL + zipCode;	
			url = new URL(u); 
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("Content-Type", "application/xml");
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"))) {
			    temp = reader.lines().collect(Collectors.joining("\n"));
			}
			doc = Jsoup.parse(temp,"",Parser.xmlParser());
		} catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println("\nOsoite: " + doc.getElementsByTag("address").text() +  " " + doc.getElementsByTag("city").text() + "\n" + doc.getElementsByTag("additionalinfo").text());
		System.out.println("Aukioloajat: " + doc.getElementsByTag("availability").text() + "\n" + doc.getElementsByTag("exceptionalavailability").text());
		System.out.println("Pup-koodi: " + doc.getElementsByTag("pupcode").text() + " " + doc.getElementsByTag("posti").text() + " " + doc.getElementsByTag("publicname").text());
		System.out.println("Reitityskoodi : " + doc.getElementsByTag("routingservicecode").text());
		System.out.println("Postinumero: " + doc.getElementsByTag("postocde").text() + " toimipaikka: " + doc.getElementsByTag("municipality").text());
		System.out.println(doc.getElementsByTag("labelname").text());
		
}
}
