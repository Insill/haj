// haj15_x1_pohja.java
// Simo Juvaste 24.1.2011/18.11.2015
// 
// Program to send X-tasks by TCP
//
// Invoke without parameters to see parameters
// 
// Tämä pohja on isommasta erilaisesta ohjelmasta viritetty, voit joko käyttää tätä
// tai jos tuntuu rakenne oudolta, niin tehdä ihan oman ohjelman
// anteeksi suomi-englanti -sekasotku....

import java.util.Scanner;
import java.io.*;
import java.net.*;
import java.security.*;
// main class

// vaihda luokan nimeksi oma käyttäjätunnuksesi
public class haj15_x1_pohja {

    // server address
    String server = "localhost";
    int port = 5555;
    Socket sock = null;
    PrintWriter out = null;
    BufferedReader in = null;

    String course = "HAJ"; // kurssin tunnus
    String taskId = "";     // tehtävän tunnus rakennetaan tähän
    String student = "";    // oma sähköpostiosoitteesi
    String passwd = "";     // oma X-lähetys salasanasi (EI UEFAD tms salasanaa)
    int task = 1;           // tehtävän numero
    int maxtask = 1;

    byte [] file = null;    // lähetettävä tiedosto tavutaulukkona
    String fileName = "";   // lähetettävän tiedoston nimi

    // tulostuksen määrä
    // 2 to see all steps
    // 1 to see some steps
    // 0 to see no steps
    int debug = 2;

    // main
    public static void main(String[] args) {
        haj15_x1_pohja o = new haj15_x1_pohja();
        o.realMain(args);
    }

    // actual work
    void realMain(String[] args) {

        try {
            getParameters(args);    // command line parameters
            checkParameters();      // check thes
            readFile();             // read input file
            check();                // check from user if ok
            connect();              // TCP connect
            sendtask();             // sending the task

        } catch (Exception e) {
            System.err.println("Exception " + e);
        } finally {
            quit();
        }
    }   // realMain()

    // muuta tämä jos et halua käyttää komentoriviparametreja
    // read parameters from command line
    void getParameters(String[] args) {

        boolean error = false;

        if (args.length != 6)
            error = true;
        else {

            // task number
            try {
                task = Integer.valueOf(args[0]);
                taskId = course + "_X" + task;
            } catch (NumberFormatException e) {
                System.err.println("Invalid task number");
                error = true;
            }

            if (task < 1 || task > maxtask) {
                System.err.println("Invalid task number");
                error = true;
            }

            // student email address
            student = args[1];
            if (student.indexOf("@") < 0) {
                System.err.println("Invalid email address");
                error = true;
            }

            // personal password
            passwd = args[2];
            if (passwd.length() < 5) {
                System.err.println("Invalid password");
                error = true;
            }

            // source file name
            fileName = args[3];
            File f = new File(fileName);
            if (! f.canRead()) {
                System.err.println("Invalid filename");
                error = true;
            }

            // server address
            server = args[4];
            try {
            InetAddress ia = InetAddress.getByName(server);
            } catch (UnknownHostException e) {
                System.err.println("Invalid server hostname");
                error = true;
            }

            // port
            try {
                port = Integer.valueOf(args[5]);
            } catch (NumberFormatException e) {
                System.err.println("Invalid port number");
                error = true;
            }
            if (port < 1 || port >= 256*256) {
                System.err.println("Invalid port number");
                error = true;
            }

        }

        if (error) {
            System.err.println("\nUsage: java vaihdaLuokanNimi # email passwd file server port");
            System.err.println("  # is the number of X-task");
            System.err.println("  email is to email you have at WebOodi or given to course");
            System.err.println("  file is your answer. Remember to have main class name as your user name");
            System.err.println("  server is the receiver server");
            System.err.println("  port is the port of the server");

            System.err.println(" E.g. java vaihdaLuokanNimi 1 uname@student.uef.fi password uname.java localhost 5556");

            throw new Xexception("Invalid parameters");
        }

    }   // getParameters()

    // let user check the parameters
    void checkParameters() {

        System.out.println("---------------------------------------------");
        System.out.println("Sending X-task");
        System.out.println("---------------------------------------------");
        System.out.println("Check following carefully:");
        System.out.println("---------------------------------------------");
        System.out.println("send server:" + server);
        System.out.println("server port:" + port);
        System.out.println("Course:" + course);
        System.out.println("Task number:" + task);
        System.out.println("Task id:" + taskId);
        System.out.println("Your email address:" + student);
        System.out.println("Your password:" + passwd);
        System.out.println("Source filename:" + fileName);
        System.out.println("---------------------------------------------\n");

        check();

    } // checkParameters()


    // TCP connection
    void connect() {

        try {
            sock = new Socket(server, port);
            out = new PrintWriter(sock.getOutputStream(), true); // L�hetykseen
            in = new BufferedReader(new InputStreamReader(sock.getInputStream())); // Vastaanottoon
            report("Connection succeeded");

        } catch (Exception e) {
            throw new Xexception("TCP connection failed: " + e);
        }
         
    } // connect()


    // main STMP conversation with server
    void sendtask() {
    	
    	out.println("Task: " + task);
    	out.println("User: " + student);
    	out.println("Passwd: " + passwd);
    	out.println(); 
    	
    	String response;
		try {
			response = in.readLine();
			System.out.println(response);
			if (response.startsWith("210"))  {
				out.println("File: " + file.length);
	    		sendFile();
	    	}
			response = in.readLine();
			System.out.println(response);
			if (response.startsWith("220")) {
				response = in.readLine();
				System.out.println(response); // Sit� varten jos palvelin tulostaa MD5-hashin
				readRest();	// Tulostetaan loput.... 
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			quit();
		}
    	
    	
    	
        
    		// TODO
        // X1 tähän


    } // sendtask()



    // apumetodeja, käytä jos haluat

    // vihjeitä:
    // kun lähetät itse tiedostoa binäärimuodossa, niin käytä 
    // BufferedOutputStream
    // esim 
    // OutputStream bout = new BufferedOutputStream(sock.getOutputStream());
    // samaa soketin OutputStream:ia voi siis käyttää kahdelle eri kuorrutuksella

    // read file contents to byte array
    void readFile() {

        try {
            RandomAccessFile f = new RandomAccessFile(fileName, "r");
            long len = f.length();

            file = new byte[(int)len];
            f.read(file);
            f.close();

        } catch (IOException e) {
            throw new Xexception("File read failed : " + e);
        }
    }

    // check user input
    void check() {
        System.out.print("Is all of above ok? (y/n) : ");

        Scanner user = new Scanner(System.in);
        String answer = user.next();

        if (answer.startsWith("y")) {
            System.out.println("Continuing\n");
            try {
            	user.close();
                Thread.sleep(1*000);
            } catch (Exception e) { }
        } else {
        	user.close();
            throw new Xexception("User canceled");
        }

    } // check()




    // send the file contents
    void sendFile() {
        try {
            OutputStream bOut = new BufferedOutputStream(sock.getOutputStream());
            // data palvelimelle
            // TODO
            bOut.write(file,0,file.length);
            bOut.flush();

        } catch (Exception e) {
            throw new Xexception("Sending file failed: " + e);
        }
        
    } // sendFile()

    void readRest() {
        try {
            // TODO

                String line = in.readLine();
                System.out.println(line); 
            } catch (Exception e) {
                throw new Xexception("readRest failed: " + e);
            }

        } 


    // close sock if it is open
    void quit() {
        try {
            if (sock != null && sock.isConnected() && !sock.isClosed())
                sock.close();
        } catch (Exception e) { }

        report("Quitting");
    } // quit()

    // print if wanted
    void report(String s) {
        if (debug > 0)
            System.out.println(s);
    }   // report

    // print if wanted more
    void report2(String s) {
        if (debug > 1)
            System.out.println(s);
    }   // report


    // own exception class
    public class Xexception extends RuntimeException {
        public Xexception() {
            super();
        }
        public Xexception(String reason) {
            super(reason);
        }
        public Xexception(Throwable t) {
            super(t);
        }
    } // class Xexception


} // class vaihdaLuokanNimi

